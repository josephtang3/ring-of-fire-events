# modified from Steven Gibbons
# use try exception else to trap FDSN exceptions
import obspy
from obspy.clients.fdsn import Client
client = Client("IRIS")
# from obspy.clients.fdsn.header import URL_MAPPINGS
# for key in sorted(URL_MAPPINGS.keys()):
#    print("{0:<11} {1}".format(key, URL_MAPPINGS[key]))
from obspy import UTCDateTime
from datetime import datetime
from obspy.clients.fdsn.header import FDSNException
print("get events around ring of fire")
# max_rad = float(str(input('Enter search radius in degrees: ')))

# NW
minlat1 = -50
maxlat1 = 60
minlon1 = 120
maxlon1 = 180

# minlat2a = -50
# maxlat2a = 0
# minlon2a = 120
# maxlon2a = 180

# SW
minlat2b = -50
maxlat2b = 0
minlon2b = -180
maxlon2b = -160

# N
minlat3 = 45
maxlat3 = 70
minlon3 = 180
maxlon3 = -105

# NE
minlat4 = 25
maxlat4 = 48
minlon4 = -128
maxlon4 = -115

# SE
minlat5 = -45
maxlat5 = -15
minlon5 = -110
maxlon5 = -55

date_string = str(input('Enter earthquake date and time UTC (yyyy-mm-dd hh:mm): '))
quake_date = datetime.strptime(date_string, "%Y-%m-%d %H:%M")
# print("quake_date string is: ", quake_date)

# network_string = str(input('Enter FDSN network name: '))
# station_string = str(input('Enter station name: '))


before_string = str(input('List how many minutes before quake: '))
# convert minutes to seconds
quake_before = int(before_string) * 60

after_string = str(input('List how many minutes after quake: '))
# print("hours string: ", after_string)
# convert minutes to seconds
quake_after = int(after_string) * 60
# print(quake_after)

t = UTCDateTime(quake_date)
[ sbef, saft ] = [ quake_before, quake_after ]
# cat = client.get_events(latitude=hk_lat, longitude=hk_long, maxradius=max_rad, starttime=t-sbef, endtime=t+saft, minmagnitude=4)

try:
	cat = client.get_events(minlat=minlat1, maxlat=maxlat1, minlon=minlon1, maxlon=maxlon1, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
except FDSNException:
	print('zone 1 No event found')
else:	
	print('zone 1')
	print(cat.__str__(print_all=True))
	cat.plot()
	
# try:
#	cat = client.get_events(minlat=minlat2a, maxlat=maxlat2a, minlon=minlon2a, maxlon=maxlon2a, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
# except FDSNException:
#	print('zone 2a No event found')
# else:	
#	print('zone 2a')
#	print(cat.__str__(print_all=True))

try:
	cat = client.get_events(minlat=minlat2b, maxlat=maxlat2b, minlon=minlon2b, maxlon=maxlon2b, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
except FDSNException:
	print('zone 2b No event found')
else:	
	print('zone 2b')
	print(cat.__str__(print_all=True))
	cat.plot()
	
try:
	cat = client.get_events(minlat=minlat3, maxlat=maxlat3, minlon=minlon3, maxlon=maxlon3, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
except FDSNException:
	print('zone 3 No event found')
else:	
	print('zone 3')
	print(cat.__str__(print_all=True))
	cat.plot()
	
try: 
	cat = client.get_events(minlat=minlat4, maxlat=maxlat4, minlon=minlon4, maxlon=maxlon4, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
except FDSNException:
	print('zone 4 No event found')
else:	
	print('zone 4')
	print(cat.__str__(print_all=True))
	cat.plot()
	
try:
	cat = client.get_events(minlat=minlat5, maxlat=maxlat5, minlon=minlon5, maxlon=maxlon5, starttime=t-sbef, endtime=t+saft, minmagnitude=4)
except FDSNException:
	print('zone 5 No event found')
else:	
	print('zone 5')
	print(cat.__str__(print_all=True))
	cat.plot()
	
# st = client.get_waveforms( network_string, station_string, "*", "B??", t - sbef, t + saft, attach_response=True)
#st.plot(linewidth=0.76, equal_scale = False)